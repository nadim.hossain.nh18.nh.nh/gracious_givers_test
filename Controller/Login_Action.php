<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login Action</title>
</head>
<body>
	<?php
		if($_SERVER['REQUEST_METHOD'] === "POST"){
			$username = $_POST['username'];
			$password = $_POST['password'];
			$rememberme = isset($_POST['rememberme']) ? $_POST['rememberme'] : null ;
			
			if(empty($username) || empty($password)){
				echo "Please Enter Username Or Password";
			}
			else{
				if(file_exists("../Model/NGO's_Data.json")){
					$handle = fopen("../Model/NGO's_Data.json", "r");
					$data = fread($handle, filesize("../Model/NGO's_Data.json"));
					$data = explode("\n", $data);
					$login = false;
					for($i = 0; $i < count($data) - 1; $i++){
						$json = json_decode($data[$i]);
						if($username === $json -> User_name and $password === $json -> Password){
							$login = true;
							break;
						}
					}
					if($login){
						if($rememberme === "on"){
							setcookie("username", $username, time() + 3600);
						}
						session_start();
						$_SESSION['username'] = $username;
						header("Location: ../View/NGO_Welcome_Page.php");
					}
					else{
						echo "Login Failed....";
					}
				}
				else{
					echo "File Not Found...";
				}
			}
		}
	?>
	<hr>
	<center> <a href="../View/NGO_Login_Page.php"> Try Again</a> </center>
	<br><br><br><br>
	<hr>
	<br>
	<?php
		include '../Controller/Footer.php';
	?>
</body>
</html>