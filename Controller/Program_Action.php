<?php

	session_start();

	$_SESSION['username'];
				
	if(file_exists("../Model/NGO's_Data.json")){
		$handle = fopen("../Model/NGO's_Data.json","r");
		$data = fread($handle,filesize("../Model/NGO's_Data.json"));
		$data = explode("\n",$data);

		for($i=0;$i< count($data) -1 ;$i++) {
			$json = json_decode($data[$i]);
						
			if($_SESSION['username'] === $json->User_name ){
				$NGO_Name = $json->NGO_Name;
			}
			else{
				
			}
		}
	}
	else{
		echo "File Not Found.";
	}
		if ($_SERVER['REQUEST_METHOD'] === "POST") {
						
			$programname = $_POST['programname'];
			$programtype = $_POST['programtype'];
			$motives = $_POST['motives'];
			$description = $_POST['description'];
			$requiredamount = $_POST['requiredamount'];
			$requiredvolunteer = $_POST['requiredvolunteer'];
			$bankaccountnumber = $_POST['bankaccountnumber'];
			$bkashnumber = $_POST['bkashnumber'];
			$startdate = $_POST['startdate'];
			$starttime = $_POST['starttime'];
			$closeddate = $_POST['closeddate'];
			$closedtime = $_POST['closedtime'];
			$programtypeErrMsg = $programnameErrMsg = $bankaccountnumberErrMsg = $bkashnumberErrMsg = '';
	
			if (!preg_match ("/^[a-z A-z ]*$/", $programname) ) {
				$programnameErrMsg = "Program name allowed only alphabets and whitespace.";
			}
	
			if (!preg_match ("/^[a-z A-z ]*$/", $programtype) ) {  
				$programtypeErrMsg = "Program type allowed only alphabets and whitespace.";
			}

			if(strlen($bankaccountnumber<8) and strlen($bankaccountnumber>12)){
				$bankaccountnumberErrMsg = "Bank Account Number Should Be Contain 8 to 12 number";
			}

			if(strlen($bkashnumber<11) and strlen($bkashnumber>11)){
				$bkashnumberErrMsg = "Bkash Number Should Be Contain 11 number";
			}
			
			if(empty($bkashnumber) || empty($bankaccountnumber) || empty($requiredvolunteer) ||
					empty($requiredamount) || empty($description) || empty($motives) || empty($programtype) || empty($programname)
					||empty($startdate) || empty($starttime) || empty($closeddate) || empty($closedtime)){
				echo " <b>Please Fill ALl Filed.</b>";
			}
			else{

				if($bkashnumberErrMsg ==='' && $bankaccountnumberErrMsg ==='' && $programnameErrMsg ==='' && $programtypeErrMsg ===''){
					$status = "pending";
					$id = rand();
					$post = "";
					$handle = fopen("../Model/Start_Program_Data.json", "a");
						$arr1 = array('id' => $id,'User_Name' => $_SESSION['username'], 'NGO_Name' => $NGO_Name, 'Program_Name' => $programname, 'Program_Type' => $programtype, 'Program_Motives' => $motives,
									'Program_Description' => $description, 'Required_Amount' => $requiredamount, 'Required_Volunteer' => $requiredvolunteer,
									'Start_Date' => $startdate,'Start_Time' => $starttime,'Closed_Date' => $closeddate,'Closed_Time' => $closedtime,
									 'Bank_Account_Number' => $bankaccountnumber, 'Bkash_Number' => $bkashnumber, 'Status' => $status, 'post' => $post);
						$json = json_encode($arr1);
						$success = fwrite($handle, $json."\n");
		
						if($success){
							echo "<br>";
							echo "Congratulations Your Program Was Started";
							$_SESSION['random'] = $id;
							echo "<br>";
							echo "<br>";
							echo "<hr>";
							echo "<br>";
							echo "<center>Please wait for admin to accepted your program. See more details go to".' <a href="../View/Pending_Program.php">pending program</a> option</center>';
						}
						else{
							echo "Something Is Missing. Try TO Start A New Program Again.";
							echo "<br>";
							echo "<hr>";
						}
		
		
						
				}
				else{
					echo "<br>";
					echo "Something Is Missing. Try TO Start A New Program Again.";
					echo "<br>";
					echo "<br>";
					echo $programtypeErrMsg;
					echo "<br>";
					echo $programnameErrMsg;
					echo "<br>";
					echo $bankaccountnumberErrMsg;
					echo "<br>";
					echo $bkashnumberErrMsg;
					echo "<br>";
					echo "<hr>";
					echo "<br>";
					echo'<a href="../View/Pending_Program.php">Try Again</a>';
					echo "<br>";
					//echo '<a href = "../View/NGO_Registrasion_Page.php"> Continue Registration </a>';
				}
			}
		}
	else{
		echo "Request Server Failed";
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Program Action</title>
</head>
<body>
	<br>
	<hr>
	<?php
		require '../Controller/Menu_Header.php';
	?>
	<br>
	<br><br><br><br><br>
	<hr>
		<br>
		<?php
		
			include '../Controller/Footer.php';
		?>
</body>
</html>