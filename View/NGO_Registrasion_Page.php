<?php

	$ngoname = "";
	$ngoownername = "";
	$pass = "";
	$ngousername = "";
	$typeofngo = "";
	$email = "";
	$confirmpassword = "";
	$url = "";
	$presentaddress = "";
	$permanentaddress = "";

	$ngonameErrMsg = "";
	$ngoownernameErrMsg = "";
	$emailErrMsg = "";
	$usernameErrMsg = "";
	$passwordErrMsg = "";
	$confirmpasswordErrMsg = "";
	$ngoownernameErrMsg = "";
	$typeofngoErrMsg = "";

	if ($_SERVER['REQUEST_METHOD'] === "POST") {

		$url = $_POST['url'];
		$presentaddress = $_POST['prenentaddress'];
		$permanentaddress = $_POST['permanentaddress'];



		if(empty($_POST['NGOname'])){
			$ngonameErrMsg = "*NGO Name is Required";
		}
		else if (!preg_match ("/^[a-z A-z ]*$/", $_POST['NGOname']) ) {  
			$ngonameErrMsg = "Only alphabets and whitespace are allowed.";
		}
		else{
			$ngoname = $_POST['NGOname'];
		}

		if(empty($_POST['NGOownername'])){
			$ngoownernameErrMsg = "*NGO Owner Name is Required";
		}
		else if (!preg_match ("/^[a-z A-z ]*$/", $_POST['NGOownername']) ) {  
			$ngoownernameErrMsg = "Only alphabets and whitespace are allowed.";
		}
		else{
			$ngoownername = $_POST['NGOownername'];
		}

		if(empty($_POST['email'])){
			$emailErrMsg = "*Email Address is Required";
		}
		else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$emailErrMsg = "Invalid email format. Email like : example@example.com";
		}
		else{
			$email = $_POST['email'];
		}

		if(empty($_POST['username'])){
			$usernameErrMsg = "*User Name is Required";
		}
		else if (!preg_match ("/^[a-zA-z0-9]*$/", $_POST['username']) ) {  
			$usernameErrMsg = "Alphabets and Numbers combination are allowed.";
		}
		else{
			$ngousername = $_POST['username'];
		}

		$number = preg_match('@[0-9]@', $_POST['password']);
		$uppercase = preg_match('@[A-Z]@', $_POST['password']);
		$lowercase = preg_match('@[a-z]@', $_POST['password']);
		$specialChars = preg_match('@[^\w]@', $_POST['password']);
		
		if(empty($_POST['password'])){
			$passwordErrMsg = "*Password is Required";
		}
		else if(strlen($_POST['password']) < 8 || !$number || !$uppercase || !$lowercase || !$specialChars) {
			$passwordErrMsg = "Password must be at least 8 characters in length and must contain at least one number, one upper case letter, one lower case letter and one special character.";
		}
		else{
			$pass = $_POST['password'];
		}

		if(empty($_POST['confirmpassword'])){
			$confirmpasswordErrMsg = "*Confirm Password is Required";
		}
		else if($_POST['password'] != $_POST['confirmpassword']){
			$confirmpasswordErrMsg = "Enter Same Password.";
		}
		else{
			$confirmpassword = $_POST['confirmpassword'];
		}

		if(isset($_REQUEST['TypeofNGO']) && $_REQUEST['TypeofNGO'] === '0') { 
			$typeofngoErrMsg = "Select Your NGO Type.";
		}
		else{
			$typeofngo = $_POST['TypeofNGO'];
		}
		
		if(empty($permanentaddress) || empty($presentaddress) || empty($url) || empty($confirmpassword) || empty($email)
				|| empty($typeofngo) || empty($ngousername) || empty($pass) || empty($ngoownername) || empty($ngoname)){
					echo "<br>";
			echo "Please Fill All Filed.";
		}
		else{

			if($ngonameErrMsg==='' && $emailErrMsg==='' && $usernameErrMsg==='' && $passwordErrMsg==='' && $confirmpasswordErrMsg === '' && $ngoownernameErrMsg === '' && $typeofngoErrMsg === ''){
				$hostname = "localhost";
				$username = "root";
				$password = "";
				$database = "ngo_database";

				$conn = new mysqli($hostname, $username, $password, $database);

				if ($conn->connect_error) {
					die("Failed to Connect: " . $conn->connect_error);
				}

				$sql = "INSERT INTO ngo_data (NGO_Name, User_Name, Ngo_Password, NGO_Owner_Name, Type_Of_NGO, Email_Address, Present_Address, Permanent_Address, NGOs_Web_Link) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				$stmt = $conn->prepare($sql);
				$stmt->bind_param('sssssssss', $NGO_Name, $User_Name, $Ngo_Password, $NGO_Owner_Name, $Type_Of_NGO, $Email_Address, $Present_Address, $Permanent_Address, $NGOs_Web_Link);
				$NGO_Name = $ngoname;
				$User_Name = $ngousername;
				$Ngo_Password = $confirmpassword;
				$NGO_Owner_Name = $ngoownername;
				$Type_Of_NGO = $typeofngo;
				$Email_Address = $email;
				$Present_Address = $presentaddress;
				$Permanent_Address = $permanentaddress;
				$NGOs_Web_Link = $url;

				if ($stmt->execute()) {
					echo "Account Created Succssfully";
				}
				else {
					echo "Something is wrong.Try again";
				}
				$conn->close();
			}
			else{
				
			}
		}
	}
	else{
		
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>NGO's Registrasion Form</title>
</head>
<body>
	<center> <h1>Welcome To NGO'S Registrasion Form</h1> </center>
	<br>
	<br>
		<form action="" method="POST">
			<fieldset>
                <legend>Account Information :</legend>
                <br>
                <span> Username : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="textarea" name="username" placeholder="User Name" <?php echo $ngousername; ?> > </span>
				<span><?php echo $usernameErrMsg ?> </span>
                <br><br>
                <span> Password : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="password" name ="password" placeholder="Password" <?php echo $pass; ?> > </span>
				<span><?php echo $passwordErrMsg ?> </span>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.One uppercase character &nbsp;&nbsp;&nbsp;&nbsp; .One special character
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.One lowercase character &nbsp;&nbsp;&nbsp;&nbsp; .8 characters minimum
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.One number
				<br><br>
				<span> Confirm Password : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="confirmpassword" name ="confirmpassword" placeholder="Confirm Password" <?php echo $confirmpassword; ?> > </span>
				<span><?php echo $confirmpasswordErrMsg ?> </span>
				<br>
				<br>
            </fieldset>
			<br>
			<fieldset>
				<legend>NGO's Information : </legend>
				<br>

				<span> NGO Name : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="NGOname" placeholder="NGO Name" <?php echo $ngoname; ?> > </span>
				<span><?php echo $ngonameErrMsg ?> </span>
				<br>
				<br>
				<span> NGO Owner Name : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="NGOownername" placeholder="NGO Owner Name" <?php echo $ngoownername; ?> > </span>
				<span><?php echo $ngoownernameErrMsg ?> </span>
				<br>
				<br>
				<span> Type Of NGO : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                <span><?php echo $typeofngoErrMsg ?> </span>
				<select class="TypeofNGO" name="TypeofNGO" <?php echo $typeofngo; ?>

                    <option>Charitable</option>
                    <option>Participatory</option>
                    <option>Service</option>
                    <option>Empowering</option>

                </select>
				<br>
				<br>
			</fieldset>
			<br>
				<fieldset>
					<legend>Contact Information :</legend>
					<br>
					Present Address: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="textarea" name="prenentaddress" placeholder="Present Address" <?php echo $presentaddress; ?> >
					<br><br>
					Permanent Address: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="textarea" name ="permanentaddress" placeholder="Permanent Address" <?php echo $permanentaddress; ?> >
					<br><br>
					<span> Email Address : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="email" name="email" placeholder="Email Address" <?php echo $email; ?> > </span>
					<span><?php echo $emailErrMsg ?> </span>
					<br><br>
					NGO'S Web Link : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="url" name="url" placeholder="NGO'S Web Link" <?php echo $url; ?> >
					<br><br>
				</fieldset>
			<br>
			Already have an account? <a href="NGO_Login_Page.php"> Login</a>
			<center><input type="submit" name="Created An Account" value="Created An Account" style="width: 15%;" ></center>
			<br>
			<center><input type="Reset" class="reset" name="reset" style="width: 15%;" /></center>
		</form>
		<br>
		<hr>
	<br>
	<?php
		
		include '../Controller/Footer.php';
		
	?>
</body>
</html>