<?php
	error_reporting(0);
	$hostname = "localhost";
	$username = "root";
	$password = "";
	$database = "ngo_database";

	$conn = new mysqli($hostname, $username, $password, $database);

	if ($conn->connect_error) {
		die("Failed to Connect: " . $conn->connect_error);
	}

	if($_SERVER['REQUEST_METHOD'] === "POST"){
		$username = $_POST['username'];
		if(empty($username)){
			echo "Please Enter Your Username";
			echo "<hr>";
		}
		else{

			$sql = "SELECT User_Name FROM ngo_data where User_Name = ? ";

			$stmt = $conn->prepare($sql);
			$stmt->bind_param('s', $User_Name);
			$User_Name = $username;

			$stmt->execute();
			$result = $stmt->get_result();

			if ($result->num_rows > 0) {

				while($row = $result->fetch_assoc()) {
					$verify = true;
				}
			}
			else{
				echo "Incorrect User Name....";
				echo "<hr>";
			}
			if($verify){
				session_start();
				$_SESSION['username'] = $username;
				header("Location: ../View/Forget_Password.php");
			}
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Verify User</title>
</head>
<body>
	<br><br><br><br><br><br>
	<form action="" method="POST">
		<fieldset>
			<legend>Verify User Name</legend>

			<center><h3>Enter your User Name To Verity Your Account.</h3></center>
			<br><br>
			<center> User Name : &nbsp;&nbsp; <input type="textarea" name="username" placeholder="User Name"<?php echo $_POST['username']??""; ?> > </center>
            <br><br>
		</fieldset>
		<br>
		<br>
		<center><input type="submit" name="Verify" value="Verify" style="width: 15%;"></center>
		<br>
	</form>
	<form action="NGO_Login_Page.php" method="POST">
	<center><input type="submit" name="Go TO Home" value="Go TO Home" style="width: 15%;"></center>
	</form>
</body>
</html>