<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Change Password</title>
</head>
<body>
<?php
	session_start();

	$uname = $_SESSION['username'];

	$hostname = "localhost";
	$username = "root";
	$password = "";
	$database = "ngo_database";

	$conn = new mysqli($hostname, $username, $password, $database);

	if ($conn->connect_error) {
		die("Failed to Connect: " . $conn->connect_error);
	}
	
	$oldpassword = "";
    $retypenew = "";
	$newpassword = "";
	$retypenewErrMsg = '';
	$newpasswordErrMsg = '';
	$oldpasswordErrMsg = '';

	if ($_SERVER['REQUEST_METHOD'] === "POST"){
			$number = preg_match('@[0-9]@', $_POST['newpassword']);
			$uppercase = preg_match('@[A-Z]@', $_POST['newpassword']);
			$lowercase = preg_match('@[a-z]@', $_POST['newpassword']);
			$specialChars = preg_match('@[^\w]@', $_POST['newpassword']);
			
			if(empty($_POST['newpassword'])){
				$newpasswordErrMsg = "*Enter Password.";
			}
			else if(strlen($_POST['newpassword']) < 8 || !$number || !$uppercase || !$lowercase || !$specialChars) {
				$newpasswordErrMsg = "Password must be at least 8 characters in length and must contain at least one number,
									one upper case letter, one lower case letter and one special character.";
			}
			else{
				$newpassword = $_POST['newpassword'];
			}
	
			if(empty($_POST['retypenew'])){
				$retypenewErrMsg = "*Enter Confirm Password";
			}
			else if($_POST['newpassword'] != $_POST['retypenew']){
				$retypenewErrMsg = "*Enter Same Password.";
			}
			else{
				$retypenew = $_POST['retypenew'];
			}

			if(empty($_POST['oldpassword'])){
				$oldpasswordErrMsg = "*Enter Old Password";
			}
			else{
				$oldpassword = $_POST['oldpassword'];
			}

			$sql1 = "SELECT Ngo_Password FROM ngo_data WHERE Ngo_Password = ?";

			$stmt1 = $conn->prepare($sql1);
			$stmt1->bind_param('s', $Ngo_Password);
			$Ngo_Password = $oldpassword;
			
			$stmt1->execute();
			$result = $stmt1->get_result();

			if ($result->num_rows > 0) {

				while($row = $result->fetch_assoc()) {

					if(mysqli_num_rows($result) === 1){
						
						if($retypenewErrMsg === '' && $newpasswordErrMsg === '' && $oldpasswordErrMsg === ''){

							$sql = "UPDATE ngo_data SET Ngo_Password = ? WHERE User_Name = ?";
			
							$stmt = $conn->prepare($sql);
							$stmt->bind_param('ss', $Ngo_Password, $User_Name);
							$Ngo_Password = $retypenew;
							$User_Name = $uname;
							if ($stmt->execute()) {
								echo "Password Change Successfull.";
								echo "<hr>";
							}
							else {
								echo "Failed to Change Password.";
								echo "<hr>";
							}
							$conn->close();
						}
						else{
							
						}
					}
					else{
						echo "Password Not Match.";
						echo "<hr>";
					}
				}
			}
			else{
				
			}
		}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Edit Profile</title>
</head>
<body>
	<br>
	<hr>
	<center> <h2>Change Password</h2></center>
	<hr>
	<?php
		require '../Controller/Menu_Header.php';
	?>
	<form action="" method="POST">
		<br>
		<center><fieldset>
			<br>
			Old Password : <input type="password" name="oldpassword" placeholder="Old Password"  <?php echo $oldpassword; ?> >
			<span><?php echo $oldpasswordErrMsg ?> </span>
			<br>
			<br>
			New Password : <input type="password" name="newpassword" placeholder="New Password"  <?php echo $newpassword; ?> >
			<span><?php echo $newpasswordErrMsg ?> </span>
			<br>
			<br>
			Re-type New : <input type="password" name="retypenew" placeholder="Confirm Password"  <?php echo $retypenew; ?> >
			<span><?php echo $retypenewErrMsg ?> </span>
			<br>
			<br>
		</fieldset></center>
		<p align="center"> <input type="submit" name="Save" value="Save Change" style="width: 10%;"></P>
	</form>
	<hr>
	<br>
	<?php
		
		include '../Controller/Footer.php';
		
	?>
</body>
</html>
</body>
</html>