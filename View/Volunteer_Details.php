<?php
	error_reporting(0);
	session_start();
	$randomnumber = $_SESSION['random'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Volunteer Details Page</title>
</head>
<body>
	<br>
	<hr>
	<center> <h2>Volunteer Details</h2></center>
	<hr>
	<?php
		require '../Controller/Menu_Header.php';
	?>
	<form action="" method="POST">
		<?php
			$handle = fopen("../Model/Selected_Volunteer.json","r");
			$data = fread($handle,filesize("../Model/Selected_Volunteer.json"));
			$data = explode("\n", $data);
			$flag = true;
			$j = 1;
			for($i=0;$i< count($data) ;$i++) {
				$json = json_decode($data[$i]);

				if( $json->id === $randomnumber){
					$_SESSION['vid'] = $json->id;
					$_SESSION['nusername'] = $json->username;
					$_SESSION['email'] = $json->email;
					$_SESSION['phone'] = $json->phone;
	
					echo "<br>";
					echo "<hr>";
					echo  "<center> $j no. Volunteer Details</center>";
					echo "<br>";
					echo "<br>";
					echo 'Name : '.$_SESSION['nusername'];
					echo "<br>";
					echo "<br>";
					echo 'Email Address : '.$_SESSION['email'];
					echo "<br>";
					echo "<br>";
					echo 'Phone Number :'.$_SESSION['phone'];
					echo "<br>";
					echo "<br>";
					echo "<hr>";
					echo "<br>";
					$j++;
				}
				else{
					$flag = false;
			   }
			}
			if($flag){
				echo "No Data..";
			}
		?>
	</form>
	<hr>
	<br>
	<?php
		
		include '../Controller/Footer.php';
		
	?>
</body>
</html>